## Sunny CTT (Camera Test Toolkit)

### sunnyuta

**UVC 摄像头测试应用**

#### 支持的功能

- 基本操作

    1. 开流

    2. 关流

    3. 采图

- 指令操作测试

    1. UVC 标注控制请求

    2. UVC 扩展通道控制请求


- 设备压测

    1. 开关流压测

    2. 软复位压测

    3. 硬复位压测


#### 使用手册
```
./build/sunnyuta -h                                                                                                                                                     [19:24:43]
Usage: ./build/sunnyuta [options] device
Supported options:
-h, --help              Show this help screen
-c, --config            Specify config file,default "./device.toml"
-d, --device            Specify camera device,default
-v, --version           Show tool version
```

运行工具时需指定需要测试的**设备名**, 设备名必须是配置文件中已经定义的, 后续的界面以及操作支持都与此绑定.


#### TODO

1. 使用LUA进行模块化 自定义操作支持
2. 使用ncurses 提高更高级的TUI界面操作
