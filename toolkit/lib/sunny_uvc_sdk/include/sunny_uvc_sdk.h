/******************************************************************************************************** 
 * 
 *  @note   : ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 *  @file   : sunny_uvc_sdk.h
 *  @brief  : Header file of the UVC sdk interface
 *  @author : zngshenbo <zngshenbo@sunnyoptical.com>
 *  @date   : createAt 2023-05-22
 * 
 *******************************************************************************************************/

#ifndef __SUNNY_UVC_SDK_H__
#define __SUNNY_UVC_SDK_H__

#include "socam_typedef.h"


typedef struct tagCamDev
{
    char cam_debug_name[64];
    char cam_id[64];
    char cam_path[256];
    char cam_name[64];
    char cam_pathEx[256];
    unsigned short vid;
    unsigned short pid;
    unsigned char bus_number;
    unsigned char device_address;
    /** USB specification release number in binary-coded decimal. A value of
     * 0x0200 indicates USB 2.0, 0x0110 indicates USB 1.1, etc. */
    unsigned short bcdUSB;

    unsigned char enable_fd;//是否使用cam_fd
    int cam_fd;//设备句柄（一般在类linux环境下才用得到，特指安卓，尤其是非root的安卓）
    const char *usbfs;
} CamDev;

//UVC video扩展单元描述符 Video Control Extension Unit Descriptor
//可通过UVCView.exe工具（或其他工具）查看到。

#define GUID_LEN (38) //字符串GUID的长度型如: "{69678EE4-410F-40DB-A850-7420D7D8240E}"
typedef struct tagSoUVCExtUnitDescriptor
{
    unsigned char bUnitID;
    char szGuidExtensionCode[GUID_LEN + 4];//
    unsigned char bNumControls;

    //其他字段有用到再添加


} SoUVCExtUnitDescriptor;

typedef enum e_tag_VC_CtrlUnit
{
    VC_QUERY_BRIGHTNESS = 0,    //stQueryVCData
    VC_GET_BRIGHTNESS,          //stVCDataValue
    VC_SET_BRIGHTNESS,          //stVCDataValue

    VC_QUERY_CONSTRAST,
    VC_GET_CONSTRAST,
    VC_SET_CONSTRAST,

    VC_QUERY_SATURATION,
    VC_GET_SATURATION,
    VC_SET_SATURATION,

    VC_QUERY_SHARPNESS,
    VC_GET_SHARPNESS,
    VC_SET_SHARPNESS,

    VC_QUERY_HUE,
    VC_GET_HUE,
    VC_SET_HUE,

    VC_QUERY_GAIN,
    VC_GET_GAIN,
    VC_SET_GAIN,

    VC_QUERY_EXP,
    VC_GET_EXP,
    VC_SET_EXP,

    VC_QUERY_WHITEBALANCE,
    VC_GET_WHITEBALANCE,
    VC_SET_WHITEBALANCE,

    VC_QUERY_ZOOM,
    VC_GET_ZOOM,
    VC_SET_ZOOM,
} e_VC_CtrlUnit;

typedef struct tagQueryVCData
{
    int max_value;
    int min_value;
    int default_value;
    int step_value;
    bool bSupAuto;
} stQueryVCData;

typedef struct tagVCDataValue
{
    int nValue;
    bool bAuto;
} stVCDataValue;

typedef struct tagVCParamCtrl
{
    e_VC_CtrlUnit nCmd;
    union
    {
        stQueryVCData stuQueryVC;
        stVCDataValue stuVCData;
    } uParam;
    tagVCParamCtrl()
    {
        memset(this, 0, sizeof(tagVCParamCtrl));
    }
} stVCParamCtrl;

typedef void(*FNCamStreamCallback)(void *buffer, int buf_len, int width, int height, unsigned int format,
                                   unsigned long long timestamp, void *user_data);

#ifdef __cplusplus
extern "C" {
#endif

/*************************************************************
Function: Sunny_SDK_Init
Description: Init sdk
Return:
    0 - succuss
Others:
*************************************************************/
 int  Sunny_SDK_Init();

/*************************************************************
Function: Sunny_SDK_Uninit
Description: Uninit sdk
Return:
    0 - succuss
Others:
*************************************************************/
 int  Sunny_SDK_Uninit();

/*************************************************************
Function: Sunny_SearchDevice
Description: Search device
Output:
    pDevsDesc - array of device description information
    pDevNum - number of supported devices searched
Return:
    0 - succuss
Others:
*************************************************************/
 int  Sunny_SearchDevice(CamDev *pDevsDesc, int *pDevNum);

/*************************************************************
Function: Sunny_Open
Description: Open camera
Input:
    fnDevStatus - device status callback function
    pUserData - user ptr
    pCamDev - camera description
Output:
    hCamDev - device handle
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_Open(HCAM *hCamDev, CamDev *pCamDev);

/*************************************************************
Function: Sunny_OpenWithFd
Description: Open camera with file description
Input:
    fd - file description
    vid - vendor id
    pid - product id
    busnum - serial number
    devaddr - devNumber
    usbfs - USBFSName
    fnDevStatus - device status callback function
    pUserData - user ptr
Output:
    hCamDev - device handle
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
#ifdef __ANDROID__
 int  Sunny_OpenWithFd(HCAM *hCamDev, int vid, int pid, int fd, int busnum, int devaddr,
                                    const char *usbfs);
#endif


/*************************************************************
Function: Sunny_StartStream
Description: Start stream
Input:
    hCamDev - device handle
    pRes - camera capability set
    fnCallback - frame callback function
    pUserData - user ptr
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_StartStream(HCAM hCamDev, CamRes *pRes, FNCamStreamCallback fnCallback, void *pUserData);

/*************************************************************
Function: Sunny_StopStream
Description: Stop stream
Input:
    hCamDev - device handle
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_StopStream(HCAM hCamDev);

/*************************************************************
Function: Sunny_Close
Description: Close device
Input:
    hCamDev - device handle
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_Close(HCAM hCamDev);

/*************************************************************
Function: Sunny_GetSdkVersion
Description: Get sdk version
Output:
    szSDKVer - sdk version, len = 128
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_GetSdkVersion(char *szSDKVer);

/*************************************************************
Function: Sunny_QueryCamCaps
Description: Get the camera capability set
Input:
    hCamDev - device handle
    nCapSize - pCamCap's size
Output:
    pCamCap - capability set
    nCapCount - set count
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_QueryCamCaps(HCAM hCamDev, CamRes *pCamCap, int nCapSize, int *nCapCount);

/*************************************************************
Function: Sunny_VcUnitTransfer
Description: Standard channel setting
Input:
    hCamDev - device handle
    nCapSize - pCamCap's size
Output/Input:
    pVCParaCtrl - Property content
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_VcUnitTransfer(HCAM hCamDev, stVCParamCtrl *pVCParaCtrl);

/*************************************************************
Function: Sunny_SetVcExtUnitConfig
Description: Extended channel configuration
Input:
    hCamDev - device handle
    pConfig - Video Control Extension Unit Descriptor
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_SetVcExtUnitConfig(HCAM hCamDev, SoUVCExtUnitDescriptor *pConfig);

/*************************************************************
Function: Sunny_VcExtUnitTransfer
Description: Extended channel transmission
Input:
    hCamDev - device handle
    dwCS - unit id
    nBufLen - data length, must be 60
    nTransferType - 0:send  1:receive
Input/Output:
    pData - transfer data, size=nBufLen, must be 60
    pdwRecvLen - receive data's length, if nTransferType == 1, this is effective
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_VcExtUnitTransfer(HCAM hCamDev, int dwCS, void *pData, int nBufLen, int nTransferType,
        unsigned int *pdwRecvLen = NULL);

/*************************************************************
Function: Sunny_SetDevBroken
Description: Set the device status to disconnected
Input:
    hCamDev - device handle
Return:
    0 - succuss     other - failed
Others:
*************************************************************/
 int  Sunny_SetDevOfflineState(HCAM hCamDev);

/*************************************************************
Function: Sunny_DevIsOn
Description: Check whether the device is online.  Only Windows and Linux users with the root permission are supported
Input:
    pCamDev - device description information
Return:
    true - online     false - offline
Others:
*************************************************************/
 bool  Sunny_DevIsOnline(CamDev *pCamDev);

/*************************************************************
Function: Sunny_GetUvcCore
Description: Return to compile platform
Return:
    0 - dshow   1 - v4l2    2 - libuvc
Others:
*************************************************************/
 int  Sunny_GetUvcCore();
#ifdef __cplusplus
}
#endif

#endif
