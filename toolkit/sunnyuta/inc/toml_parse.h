#ifndef _TOML_PARSE_H
#define _TOML_PARSE_H

#include "sunnyuta.h"

int toml_config_parse(char *config_path, cam_dev_t *cam_dev, uint32_t* device_num);
#endif