/**
 * @file function_operation.h
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief independent device function operation
 * @version 0.1
 * @date 2023-07-25
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */
#ifndef __FUNC_OP_H
#define __FUNC_OP_H

#include "sunnyuta.h"
#include "sunny_uvc_sdk.h"

/**
 * @brief 
 * 
 * @param handle_name 
 * @param functionAttr 
 * @return int 
 */
int function_GetFunctionAttrWithName(const char * handle_name, function_attr_t * functionAttr);

/**
 * @brief 
 * 
 * @param hanlde_name 
 * @param handle 
 * @return int 
 */
int function_GetHandleWithName(const char * hanlde_name, cam_operation_handle_t *handle);

/**
 * @brief 
 * 
 * @param handle_name 
 * @param phDev 
 * @param pstCam 
 * @param op_idx 
 * @return int 
 */
int function_ExecHandleByName(const char *handle_name, HCAM *phDev, cam_dev_t *pstCam, uint32_t op_idx);
#endif
