#ifndef __SUNNYUTA_H
#define __SUNNYUTA_H

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <malloc.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/types.h> 
#include <sys/select.h>
#include <dirent.h>
#include <stdarg.h>
#include <stdlib.h>
#include <curses.h>
#include <signal.h>
#include <pthread.h>

#include <sunny_uvc_sdk.h>

#define SUNNYUTA_VERSION    "0.0.1"
#define SUNNYUTA_AUTHOR     "zngyanj@sunnyoptical.com"

#define SUNNYUTA_DEV_NUM_MAX 10
#define SUNNYUTA_OPT_LIST_IDX_MAX 10

/* tool software componcontes macro*/
#define USING_NCURSES 0

/* device specified macro */
#define PALM_DEVICE_XU_ENABLE   1

typedef struct cam_usb_id {
    uint16_t vid;
    uint16_t pid;
} cam_usb_id_t;

typedef struct cam_video_attr {
    uint32_t width;
    uint32_t height;
    uint32_t frameRate;
    uint32_t format;
} cam_video_attr_t;

typedef int (*cam_operation_handle_t)(struct cam_operation_args *);

typedef struct cam_xu_operation_args {
    char type[8];
    uint8_t uint_id;
    uint32_t control_selector;
    uint32_t data;
} cam_operation_config_t;

typedef enum function_type {
    FUNCTION_EXEC_ONCE = 0,
    FUNCTION_EXEC_LOOP = 1,
} function_type_e;

typedef struct function_attr{
    const char *function_name;
    cam_operation_handle_t op_handle;
    function_type_e function_type;
    pthread_t Tid;
    uint32_t ThreadStart;
} function_attr_t;

typedef struct function_arg {
    HCAM *hDev;
    struct cam_dev *cam_dev;
    uint32_t op_idx;
    ssize_t data_len;
    uint8_t* data;
} function_arg_t;

typedef struct cam_operation_args {
    HCAM *hDev;
    struct cam_dev *cam_dev;
    uint32_t op_idx;
    ssize_t data_len;
    uint8_t* data;
    cam_operation_handle_t * phandle;
    function_attr_t *func_attr;
} cam_operation_args_t;


typedef struct cam_operation {
    char name[64];
    cam_operation_handle_t handle;
    cam_operation_config_t config;
    cam_operation_args_t   args;
} cam_operation_t;

typedef struct cam_op_list {
    uint32_t supported_opt_num;
    cam_operation_t operation_list[SUNNYUTA_OPT_LIST_IDX_MAX];
} cam_op_list_t;

typedef struct cam_dev {
    char name[64];
    int32_t ok;                /* usb device ok */
    int32_t enable;            /* configuration enable/disable */
    cam_usb_id_t usb_id;
    cam_video_attr_t video_attr;
    cam_op_list_t operationes;
} cam_dev_t;

typedef struct sunnyuta_conf {
    char * dev_name;
    char * config_filename;
} sunnyuta_conf_t;
#endif