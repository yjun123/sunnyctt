/**
 * @file ui_interaction.h
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief 
 * @version 0.1
 * @date 2023-06-28
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */

#ifndef __UI_INTERAC_H
#define __UI_INTERAC_H

#include "sunnyuta.h"

/**
 * @brief show all device which is detected
 * 
 * @param dev 
 * @param dev_num 
 * @return int 
 */
int ui_show_device(cam_dev_t *dev, uint32_t dev_num);

/**
 * @brief demo,only get one number
 * 
 * @param tips 
 * @param string_format 
 * @param d 
 * @return int 
 */
int ui_get_single_number(const char * tips, const char * string_format, void *d);

/**
 * @brief show device operation
 * 
 * @param dev 
 * @return int 
 */
int ui_show_operation(cam_dev_t *dev);



/**
 * @brief UI Initialization
 * 
 * @return int 
 */
int ui_init(void);

#endif