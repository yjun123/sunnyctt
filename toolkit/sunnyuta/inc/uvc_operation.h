/**
 * @file uvc_operation.h
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief 
 * @version 0.1
 * @date 2023-06-28
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */

#ifndef __UVC_OP_H
#define __UVC_OP_H

#include "sunnyuta.h"
#include "sunny_uvc_sdk.h"

typedef struct UVC_XU_CMD_Packet UVC_XU_CMD_Packet_t;
typedef struct UVC_XU_RSP_Packet UVC_XU_RSP_Packet_t;

/**
 * @brief UVC relatied initialization
 * 
 * @return int 
 */
int uvc_init(void);

/**
 * @brief uVC relatied deinitialization
 * 
 * @return int 
 */
int uvc_deinit(void);


/**
 * @brief UVC open deivice operatioin
 * 
 * @param dev 
 * @param hDev 
 * @return int 
 */
int uvc_open(HCAM *hDev, cam_dev_t *dev);

/**
 * @brief 
 * 
 * @param hDev 
 * @return int 
 */
int uvc_close(HCAM hDev);

/**
 * @brief UVC start capture operation
 * 
 * @param hDev 
 * @param dev 
 * @return int 
 */

int uvc_startStream(HCAM hDev, cam_dev_t *dev);

/**
 * @brief UVC stop stream
 * 
 * @param hDev 
 * @param dev 
 * @return int 
 */
int uvc_stopStream(HCAM hDev, cam_dev_t *dev __attribute__((unused)));

/**
 * @brief check uvc device
 * 
 * @param dev 
 * @return int 
 */
int uvc_is_existed(cam_dev_t *dev);

/**
 * @brief 
 * 
 * @param hDev 
 * @param __attribute__ 
 * @return int 
 */
int uvc_captureFrame(HCAM hDev, cam_dev_t *dev __attribute__((unused)));

/**
 * @brief 
 * 
 * @param hDev 
 * @param __attribute__ 
 * @param args 
 * @param XU_CMD_Packet 
 * @param CMD_Packet_size 
 * @param XU_RSP_Packet 
 * @param RSP_Packet_size 
 * @return int 
 */
int uvc_controlXU(HCAM hDev, cam_dev_t *dev __attribute__((unused)), cam_operation_config_t *args, UVC_XU_CMD_Packet_t *XU_CMD_Packet, ssize_t CMD_Packet_size, UVC_XU_RSP_Packet_t *XU_RSP_Packet, ssize_t RSP_Packet_size);
#endif  