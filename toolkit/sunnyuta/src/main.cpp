
#include "debug.h"
#include "sunnyuta.h"
#include "toml_parse.h"
#include "uvc_operation.h"
#include "ui_interaction.h"
#include "function_operation.h"

#define DEFALUT_CONFIG_NAME "/device.toml"

static cam_dev_t g_devices[SUNNYUTA_DEV_NUM_MAX] = {0};
static uint32_t g_device_num = 0;

static int usage(char *name)
{
    SY_DBG("Usage: %s [options] device\n", name);
    SY_DBG("Supported options:\n");
    SY_DBG("-h, --help              Show this help screen\n");
    SY_DBG("-c, --config            Specify config file,default \"./device.toml\"\n");
    SY_DBG("-d, --device            Specify camera device,default\n");
    SY_DBG("-v, --version           Show tool version\n");

    return 0;
}

static int version(char *name)
{
    SY_DBG("Version: %s\n", SUNNYUTA_VERSION);
    SY_DBG("Author: %s\n", SUNNYUTA_AUTHOR);

    return SUCCESS;
}

static int sy_misc_get_dirpath(char *path, char *dirname)
{
    char *ptr = NULL;
    ptr = strrchr(path,'/');
    if (!ptr) {
        memcpy(dirname, path,strlen(path));
    } else {
        memcpy(dirname, path,ptr - path);
        dirname[ptr-path] = '\0';
    }
    return SUCCESS;
}

static int get_exe_abs_path(char * buf, ssize_t max_size)
{
    ssize_t len = readlink("/proc/self/exe", buf, max_size -1);

    SY_CHECK(len != -1, SY_ERR("read exe abs path error: %s\n", strerror(errno)); goto __ret);

    SY_CHECK(!sy_misc_get_dirpath(buf, buf), goto __ret);

    SY_DBG("exe abs path = %s\n", buf);
    return SUCCESS;

__ret:
    return FAILURE;
}

static int logic_loop(char * dev_name)
{
    uint32_t cam_idx;
    cam_dev_t* pstCam = NULL;
    HCAM hDev;
    cam_operation_handle_t * pHandle = NULL;
    cam_operation_t * pOpt = NULL;

    int32_t idx;
    uint32_t ok_device = 0;

__dev_check:
    // device detection check
    for(int i = 0; i < g_device_num; i++)
    {
        if(dev_name) // 由指定设备
        { 
            if(!strcmp(dev_name, g_devices[i].name) && !uvc_is_existed(&g_devices[i]))
            {
                SY_DBG("dev %s is live\n", g_devices[i].name);
                g_devices[i].ok = 1;
                ok_device ++;
            }
            else
                g_devices[i].ok = 0;
        }
        else
        {
            if(!uvc_is_existed(&g_devices[i]))
                g_devices[i].ok = 1;
            else
                g_devices[i].ok = 0;
        }
    }
    
    // SY_CHECK(ok_device > 0, SY_ERR("No device Found\n"); goto __dev_check);

__dev_select:
#if defined(USING_NCURSES) && USING_NCURSES == 0
    ui_show_device(g_devices, g_device_num);

    ui_get_single_number("enter the index number> ", "%d", &idx);

    // SY_DBG("idx of input is %u\n", d_idx);
    
    /* Refresh device list */
    if(idx == g_device_num) goto __dev_check;

    SY_CHECK(idx < g_device_num && idx >= 0, SY_ERR("device idx is invalid\n"); return FAILURE);

    cam_idx = idx;
    pstCam = &g_devices[cam_idx];

    SY_CHECK(!uvc_open(&hDev, pstCam), return FAILURE);

    while(idx >= 0){
        ui_show_operation(pstCam);
        ui_get_single_number("enter the index numer> ", "%d", &idx);
        SY_CHECK(idx < (int32_t)pstCam->operationes.supported_opt_num, return FAILURE);
        if(idx < 0)
        {
            INFO("Exit %s\n", pstCam->name);
            goto __dev_select;
        }
        pOpt = &pstCam->operationes.operation_list[idx];
        INFO("exec %s -> %s\n", pstCam->name, pOpt->name);
        SY_CHECK(!function_ExecHandleByName(pOpt->name, &hDev, pstCam, idx), return FAILURE);
        
    }
#else
    ui_init();
#endif

    return SUCCESS;
}

int main(int argc, char **argv)
{
    int option;
    char * device_name = NULL;
    char * config_file_name = (char *)DEFALUT_CONFIG_NAME;
    char exe_abs_path[1024] = {0};

    static struct option long_options[] = {                                                                                                                                                                                      
        {"help", no_argument, 0, 'h' },
        {"config", optional_argument, 0, 'c'},                                                                                                                                                                                    
        {"device", optional_argument, 0, 'd'},                                                                                                                                                                                      
        {"version", no_argument, 0, 'v'},                                                                                                                                
        {0, 0, 0, 0}                                                                                                                                                                                                             
    };

    while((option = getopt_long(argc, argv, "hd::v", long_options, NULL)) != -1)
    {
        switch(option)
        {
            case 0:
                break;
            case 'c':
                if(NULL != optarg)
                    config_file_name = optarg;
                break;
            case 'd':
                if(NULL != optarg)
                    device_name = optarg;
                break;
            case 'h':
                return usage(argv[0]);
                break;
            case 'v':
                return version(argv[0]);
                break;
            default:
                break;
        }
    }

    if(optind < argc and NULL != argv[optind])
    {
        device_name = argv[optind];
    }

    // if(NULL == device_name)
    // {
    //     SY_ERR("the device must be specified but is missing!\n");
    //     return usage(argv[0]);
    // }

    /* Initialization */
    SY_CHECK(!uvc_init(), goto __fail);

    get_exe_abs_path(exe_abs_path, sizeof(exe_abs_path));

    SY_DBG("Got device name = %s\n", device_name);
    SY_DBG("Got config file name = %s\n", config_file_name);

    strcat(exe_abs_path, config_file_name);
    SY_DBG("config file abs path = %s\n", exe_abs_path);
    SY_CHECK(!access(exe_abs_path, F_OK|R_OK), SY_ERR("config \"%s\" is not accessable\n", exe_abs_path); goto __fail);

    SY_CHECK(!toml_config_parse(exe_abs_path, g_devices, &g_device_num), goto __fail);

    for(int i = 0; i < g_device_num; i++)
    {
        SY_DBG("dev -> name = %s\n", g_devices[i].name);
        SY_DBG("dev -> enable = %s\n", g_devices[i].enable ? "true" : "false");
        SY_DBG("dev -> usb_id\n");
        SY_DBG("dev -> usb_id -> vid = %u\n", g_devices[i].usb_id.vid);
        SY_DBG("dev -> usb_id -> pid = %u\n", g_devices[i].usb_id.pid);
        SY_DBG("dev -> video_attr\n");
        SY_DBG("dev -> video_attr -> format = %u\n", g_devices[i].video_attr.format);
        SY_DBG("dev -> video_attr -> frameRate= %u fps\n", g_devices[i].video_attr.frameRate);
        SY_DBG("dev -> video_attr -> width = %u pixels\n", g_devices[i].video_attr.width);
        SY_DBG("dev -> video_attr -> height = %u pixels\n", g_devices[i].video_attr.height);
        SY_DBG("dev -> operation\n");
        SY_DBG("dev -> operation -> supported_opt_num = %d\n", g_devices[i].operationes.supported_opt_num);
        for(int p_idx = 0; p_idx < g_devices[i].operationes.supported_opt_num; p_idx ++)
        {
            SY_DBG("dev -> operation idx %d = %s\n", p_idx, g_devices[i].operationes.operation_list[p_idx].name);
            SY_DBG("dev -> operation idx %d -> args\n", p_idx);
            SY_DBG("dev -> operation idx %d -> args -> type = %s\n", p_idx, g_devices[i].operationes.operation_list[p_idx].config.type);
            SY_DBG("dev -> operation idx %d -> args -> uint_id = %d\n", p_idx, g_devices[i].operationes.operation_list[p_idx].config.uint_id);
            SY_DBG("dev -> operation idx %d -> args -> control_selector = %d\n", p_idx, g_devices[i].operationes.operation_list[p_idx].config.control_selector);
            SY_DBG("dev -> operation idx %d -> args -> data = %u\n", p_idx, g_devices[i].operationes.operation_list[p_idx].config.data);
        }
    }

    SY_CHECK(!logic_loop(device_name), return FAILURE);

    /* Deinitialization */
    SY_CHECK(!uvc_deinit(), goto __fail);
    
    return SUCCESS;
__fail:
    return FAILURE;
}
