/**
 * @file toml_parse.cpp
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief parse toml config
 * @version 0.1
 * @date 2023-06-28
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */
#include "debug.h"
#include "sunnyuta.h"
#include "sunny_uvc_sdk.h"
#include "toml.h"
#include "function_operation.h"


static int toml_parse_operation_args(toml_table_t *table,cam_operation_t *operation)
{
    const char *attr_key = NULL;
    const char *attr_subkey = NULL;
    toml_datum_t attr_data;
    toml_table_t *attr_table;



    // parse type
    attr_key = "type";
    attr_data = toml_string_in(table, attr_key);
    SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] for operation %s\n", attr_key, operation->name);
                            goto __fail);
    strcpy(operation->config.type, attr_data.u.s);
    
    attr_subkey = "attr";
    SY_CHECK(toml_key_exists(table, attr_subkey), 
            SY_ERR("missing [%s] in %s for operation %s\n", attr_subkey,  attr_key , operation->name);
            goto __fail);
    attr_table = toml_table_in(table, attr_subkey);
    SY_CHECK(attr_table, SY_ERR("failed to parse [%s] for operation %s\n", attr_key, operation->name);
                            goto __fail);

    if(!strcmp(operation->config.type, "GENERIC"))
    {
        // parse data
        attr_subkey = "data";
        attr_data = toml_int_in(attr_table, attr_subkey);
        SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] for operation %s\n", attr_subkey, operation->name);
                        goto __fail);
        operation->config.data = attr_data.u.i;
    }
    else if(!strcmp(operation->config.type, "XU"))
    {
        // parse uint_id
        attr_subkey = "uint_id";
        attr_data = toml_int_in(attr_table, attr_subkey);
        SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] for operation %s\n", attr_subkey, operation->name);
                                goto __fail);
        operation->config.uint_id = (uint8_t)attr_data.u.i;

        // parse control_selector
        attr_subkey = "control_selector";
        attr_data = toml_int_in(attr_table, attr_subkey);
        SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] for operation %s\n", attr_subkey, operation->name);
                                goto __fail);
        operation->config.control_selector = (uint8_t)attr_data.u.i;

        // parse data
        attr_subkey = "data";
        attr_data = toml_int_in(attr_table, attr_subkey);
        SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] for operation %s\n", attr_subkey, operation->name);
                                goto __fail);
        operation->config.data = attr_data.u.i;
    }

    return SUCCESS;
__fail:
    return FAILURE;
}

int toml_config_parse(char *config_path, cam_dev_t *cam_dev, uint32_t* device_num)
{
    FILE *fp = NULL;
    char errbuf[200];
    *device_num = 0;

    toml_table_t *conf, *device, *operation;

    const char *device_key = NULL;
    toml_table_t *device_table = NULL;
    const char *attr_key = NULL;
    const char *attr_sub_key = NULL;
    toml_table_t *attr_table = NULL;
    toml_datum_t attr_data;

    const char *operation_key = NULL;
    toml_table_t *operation_table = NULL;
    toml_array_t *operation_list = NULL;
    toml_table_t *operation_arg_table = NULL;

    fp = fopen(config_path, "r");
    SY_CHECK(fp, SY_ERR("cannot open %s config file - %s\n", config_path,strerror(errno));
                    goto __fail);
    
    conf = toml_parse_file(fp, errbuf, sizeof(errbuf));
    SY_CHECK(conf, SY_ERR("cannot parse config - %s", errbuf);
                    goto __fail);

    /* get device */
    device = toml_table_in(conf, "device");
    SY_CHECK(device, SY_ERR("missing [device]\n");
                    goto __fail);
    /* get operation */
    operation = toml_table_in(conf, "operation");
    SY_CHECK(operation, SY_ERR("missing [operation]\n");
                    goto __fail);
    /* parse Device */
    SY_DBG("Supported Cam:\n");
    for(int i = 0; NULL != (device_key = toml_key_in(device, i)); i++)
    {
        if(NULL != (device_table = toml_table_in(device, device_key)))
        {
            SY_DBG("Got %s table\n", device_key);
            strcpy(cam_dev[*device_num].name, device_key);

            /* parse usbid */
            attr_key = "usbid";
            SY_CHECK(toml_key_exists(device_table, attr_key), 
                    SY_ERR("missing [%s] for device %s\n", attr_key ,device_key);
                    goto __fail);
            attr_table = toml_table_in(device_table, attr_key);
            SY_CHECK(attr_table, SY_ERR("failed to parse [%s] for device %s\n", attr_key, device_key);
                                    goto __fail);
            // parse pid
            attr_sub_key = "pid";
            SY_CHECK(toml_key_exists(attr_table, attr_sub_key),
                        SY_ERR("missing [%s] in [%s] for device %s\n",attr_sub_key, attr_key ,device_key);
                        goto __fail);
            attr_data = toml_int_in(attr_table, attr_sub_key);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] in [%s] for device %s\n", attr_sub_key,attr_key, device_key);
                                    goto __fail);
            cam_dev[*device_num].usb_id.pid = attr_data.u.i;
            // parse vid
            attr_sub_key = "vid";
            SY_CHECK(toml_key_exists(attr_table, attr_sub_key),
                        SY_ERR("failed to parse [%s] in [%s] for device %s\n", attr_sub_key,attr_key, device_key);
                        goto __fail);
            attr_data = toml_int_in(attr_table, attr_sub_key);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] in [%s] for device %s\n", attr_sub_key,attr_key, device_key);
                                    goto __fail);
            cam_dev[*device_num].usb_id.vid = attr_data.u.i;


            /* parse frame */
            attr_key = "frame";
            SY_CHECK(toml_key_exists(device_table, attr_key),
                        SY_ERR("missing [%s] for device %s\n",attr_key ,device_key);
                        goto __fail);
            attr_table = toml_table_in(device_table, attr_key);
            SY_CHECK(attr_table, SY_ERR("failed to parse [%s] for device %s\n", attr_key,device_key);
                                goto __fail);
            // parse width
            attr_sub_key = "width";
            attr_data = toml_int_in(attr_table, attr_sub_key);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] in [%s] for device %s\n", attr_sub_key, attr_key, device_key);
                                    goto __fail);
            cam_dev[*device_num].video_attr.width = attr_data.u.i;
            
            // parse height
            attr_sub_key = "height";
            attr_data = toml_int_in(attr_table, attr_sub_key);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] in [%s] for device %s\n",  attr_sub_key, attr_key, device_key);
                                    goto __fail);
            cam_dev[*device_num].video_attr.height = attr_data.u.i;
            // parse fps
            attr_sub_key = "fps";
            attr_data = toml_int_in(attr_table, attr_sub_key);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] in [%s] for device %s\n", attr_sub_key, attr_key, device_key);
                                    goto __fail);
            cam_dev[*device_num].video_attr.frameRate = attr_data.u.i;
            
            /* parse format */
            attr_key = "format";
            SY_CHECK(toml_key_exists(device_table, attr_key),
                        SY_ERR("missing [%s] for device %s\n", attr_key, device_key);
                        goto __fail);
            attr_data = toml_string_in(device_table, attr_key);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] for device %s\n", attr_key, device_key);
                                    goto __fail);
            if(!strcmp(attr_data.u.s, "YUY2") || !strcmp(attr_data.u.s, "yuy2"))
                cam_dev[*device_num].video_attr.format = SOCAM_STREAM_FMT_YUY2;
            else if(!strcmp(attr_data.u.s, "MJPEG") || !strcmp(attr_data.u.s, "mjpeg"))
                cam_dev[*device_num].video_attr.format = SOCAM_STREAM_FMT_H264;
            else if (!strcmp(attr_data.u.s, "H264") || !strcmp(attr_data.u.s, "h264"))
                cam_dev[*device_num].video_attr.format = SOCAM_STREAM_FMT_H264;
            else if (!strcmp(attr_data.u.s, "YUYV") || !strcmp(attr_data.u.s, "yuyv"))
                cam_dev[*device_num].video_attr.format = SOCAM_STREAM_FMT_YUYV;
            // else if (!strcmp(attr_data.u.s, "H265") || !strcmp(attr_data.u.s, "h265"))
            //     cam_dev[*device_num].video_attr.format = SOCAM_STREAM_FMT_H265;
            else if (!strcmp(attr_data.u.s, "NV12") || !strcmp(attr_data.u.s, "nv12"))
                cam_dev[*device_num].video_attr.format = SOCAM_STREAM_FMT_NV12;
            else {
                SY_ERR("Wrong format string %s\n", attr_data.u.s);
                goto __fail;
            }
            free(attr_data.u.s);

            /* parse ok */
            attr_key = "enable";
            SY_CHECK(toml_key_exists(device_table, attr_key),
                        SY_ERR("missing [%s] for device %s\n", attr_key, device_key);
                        goto __fail);
            attr_data = toml_bool_in(device_table, attr_key);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse [%s] for device %s\n", attr_key, device_key);
                                    goto __fail);
            if(attr_data.u.b)
                cam_dev[*device_num].enable = 1;
            else
                cam_dev[*device_num].enable = 0;
        }
        (*device_num) ++;
    }

    SY_CHECK(*device_num >= 1, SY_ERR("device_num = %n, is less than 1, failed\n", device_num);
                                goto __fail);

    /* parse Operation */

    SY_DBG("Supported Operation:\n");
    operation_key = "generic";
    if(NULL != (operation_table = toml_table_in(operation, operation_key)))
    {
        SY_DBG("Got %s table\n", operation_key);

        /* parse op_list */
        attr_key = "op_list";
        SY_CHECK(toml_key_exists(operation_table, attr_key),
                    SY_ERR("missing [%s] for opertion %s\n", attr_key, operation_key);
                    goto __fail);
        operation_list = toml_array_in(operation_table, "op_list");
        SY_CHECK(operation_list, SY_ERR("failed to parse [%s] for operation %s\n", attr_key, operation_key);
                                return  FAILURE);

        // parse operation instance
        int num_of_element = toml_array_nelem(operation_list);
        SY_CHECK(num_of_element >=1 , SY_ERR("the num of operation %s is %d", attr_key, num_of_element);
                                        goto __fail);

        for(int ii =0; ii < num_of_element; ii++)
        {
            attr_data = toml_string_at(operation_list, ii);
            SY_CHECK(attr_data.ok, SY_ERR("failed to parse element idx %d in [%s] for operation %s\n", ii, attr_key, operation_key);
                    goto __fail);
            SY_DBG("%s table: idx %d = %s\n", operation_key, ii, attr_data.u.s);

            for(int d = 0; d < *device_num; d++)
            {
                uint32_t op_idx = cam_dev[d].operationes.supported_opt_num++;
                strcpy(cam_dev[d].operationes.operation_list[op_idx].name, attr_data.u.s);
                SY_CHECK(!function_GetHandleWithName(cam_dev[d].operationes.operation_list[op_idx].name, &cam_dev[d].operationes.operation_list[op_idx].handle), return FAILURE);
                SY_CHECK(cam_dev[d].operationes.operation_list[op_idx].handle, return FAILURE);

                cam_operation_config_t *pstrArgs = &cam_dev[d].operationes.operation_list[op_idx].config;
                
                attr_key = cam_dev[d].operationes.operation_list[op_idx].name;
                SY_CHECK(toml_key_exists(operation_table, attr_key),
                            SY_ERR("missing [%s] for opertion %s\n", attr_key, operation_key);
                            goto __fail);
                operation_arg_table = toml_table_in(operation_table, attr_key);
                SY_CHECK(operation_arg_table, SY_ERR("failed to parse arg of operation %s for device %s\n", attr_key, cam_dev[d].name);
                                            return  FAILURE);
                
                toml_parse_operation_args(operation_arg_table, &cam_dev[d].operationes.operation_list[op_idx]);
            }
        }
    }
    
    /* 遍历设备特定的操作 */
    for(int d = 0; d < *device_num; d++)
    {
        operation_key = cam_dev[d].name;
        if(NULL != (operation_table = toml_table_in(operation, operation_key)))
        {
            SY_DBG("Got %s table\n", operation_key);

            if(!cam_dev[d].enable)
                continue;

            /* parse op_list */
            attr_key = "op_list";
            SY_CHECK(toml_key_exists(operation_table, attr_key),
                        SY_ERR("missing [%s] for opertion %s\n", attr_key, operation_key);
                        goto __fail);
            operation_list = toml_array_in(operation_table, attr_key);
            SY_CHECK(operation_list, SY_ERR("failed to parse [%s] for operation %s\n", attr_key, operation_key);
                                    return  FAILURE);

            // parse operation instance
            int num_of_element = toml_array_nelem(operation_list);
            SY_CHECK(num_of_element >=1 , SY_ERR("the num of operation %s is %d", attr_key, num_of_element);
                                            goto __fail);

            for(int ii =0; ii < num_of_element; ii++)
            {
                attr_data = toml_string_at(operation_list, ii);
                SY_CHECK(attr_data.ok, SY_ERR("failed to parse element idx %d in [%s] for operation %s\n", ii, attr_key, operation_key);
                        goto __fail);
                SY_DBG("%s table: idx %d = %s\n", operation_key, ii, attr_data.u.s);

                {
                    uint32_t op_idx = cam_dev[d].operationes.supported_opt_num ++;
                    strcpy(cam_dev[d].operationes.operation_list[op_idx].name, attr_data.u.s);
                    SY_CHECK(!function_GetHandleWithName(cam_dev[d].operationes.operation_list[op_idx].name, &cam_dev[d].operationes.operation_list[op_idx].handle), return FAILURE);
                    SY_CHECK(cam_dev[d].operationes.operation_list[op_idx].handle, return FAILURE);

                    attr_key = cam_dev[d].operationes.operation_list[op_idx].name;
                    SY_CHECK(toml_key_exists(operation_table, attr_key),
                                SY_ERR("missing [%s] for opertion %s\n", attr_key, operation_key);
                                goto __fail);
                    operation_arg_table = toml_table_in(operation_table, attr_key);
                    SY_CHECK(operation_arg_table, SY_ERR("failed to parse arg of operation %s for device %s\n", attr_key, cam_dev[d].name);
                                                return  FAILURE);
                    
                    toml_parse_operation_args(operation_arg_table, &cam_dev[d].operationes.operation_list[op_idx]);
                }
            }
        }
    }

    toml_free(conf);
    fclose(fp);
    return SUCCESS;

__fail:
    toml_free(conf);
    fclose(fp);
    return FAILURE;
}
