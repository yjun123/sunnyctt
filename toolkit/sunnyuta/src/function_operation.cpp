/**
 * @file function_operation.cpp
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief independent device function operation
 * @version 0.1
 * @date 2023-07-25
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */

#include "debug.h"
#include "sunnyuta.h"
#include "sunny_uvc_sdk.h"
#include "uvc_operation.h"
#include "function_operation.h"
#if PALM_DEVICE_XU_ENABLE
#include "palm.h"
#endif


/************************************************************************** FUNCTION HANDLE ***************************************************8*/
/**
 * @brief StartCapture
 * 
 * @param hDev 
 * @param dev 
 * @return int 
 */
static int function_StartCapture(cam_operation_args_t * args)
{
    SY_CHECK(!uvc_startStream(*args->hDev,args->cam_dev), return FAILURE);

    return SUCCESS;
}


/**
 * @brief StopCapture
 * 
 * @param hDev 
 * @param dev 
 * @return int 
 */
int function_StopCapture(cam_operation_args_t * args)
{
    SY_CHECK(!uvc_stopStream(*args->hDev, NULL), return SUCCESS);
    
    return SUCCESS;
}


/**
 * @brief 
 * 
 * @param hanlde_name 
 * @param handle 
 * @return int 
 */
static int function_CaptureFrame(cam_operation_args_t * args)
{
    SY_CHECK(!uvc_captureFrame(*args->hDev,args->cam_dev), return FAILURE);
    
    return SUCCESS;
}


/**
 * @brief 
 * 
 * @param hanlde_name 
 * @param handle 
 * @return int 
 */
static int function_GetDeviceSN(cam_operation_args_t * args)
{
    INFO("enter %s\n", __func__);

    PALM_XU_CMD_Packet_t XU_CMD_Packet;
    PALM_XU_RSP_Packet_t XU_RSP_Packet;

    uint32_t idx = args->op_idx;
    int32_t cmd = *(uint32_t *) args->data;
    SY_CHECK(args->data_len == sizeof(cmd), return FAILURE);

    cam_operation_config_t *op_config = &args->cam_dev->operationes.operation_list[idx].config;

    SY_CHECK(!PALM_XU_BuildCmdPacket(&XU_CMD_Packet, (uint8_t)cmd, NULL, 0), return FAILURE);
    SY_CHECK(!uvc_controlXU(*args->hDev,args->cam_dev, op_config, (UVC_XU_CMD_Packet_t *)&XU_CMD_Packet, sizeof(PALM_XU_CMD_Packet_t), 
                                    (UVC_XU_RSP_Packet_t *)&XU_RSP_Packet, sizeof(PALM_XU_RSP_Packet_t)),
                                    return FAILURE);
    SY_CHECK(!PALM_XU_CheckRspPacket(&XU_RSP_Packet), return FAILURE);
    
    SY_DBG("%s: result = %s\n",__func__, (char *)XU_RSP_Packet.data);

    return SUCCESS;
}

/**
 * @brief 
 * 
 * @param hanlde_name 
 * @param handle 
 * @return int 
 */
static int function_GetFirmwareVersion(cam_operation_args_t * args)
{
    INFO("enter %s\n", __func__);

    PALM_XU_CMD_Packet_t XU_CMD_Packet;
    PALM_XU_RSP_Packet_t XU_RSP_Packet;

    uint32_t idx = args->op_idx;
    int32_t cmd = *(uint32_t *) args->data;
    SY_CHECK(args->data_len == sizeof(cmd), return FAILURE);

    cam_operation_config_t *op_config = &args->cam_dev->operationes.operation_list[idx].config;

    SY_CHECK(!PALM_XU_BuildCmdPacket(&XU_CMD_Packet, (uint8_t)cmd, NULL, 0), return FAILURE);
    SY_CHECK(!uvc_controlXU(*args->hDev,args->cam_dev, op_config, (UVC_XU_CMD_Packet_t *)&XU_CMD_Packet, sizeof(PALM_XU_CMD_Packet_t), 
                                    (UVC_XU_RSP_Packet_t *)&XU_RSP_Packet, sizeof(PALM_XU_RSP_Packet_t)),
                                    return FAILURE);
    SY_CHECK(!PALM_XU_CheckRspPacket(&XU_RSP_Packet), return FAILURE);
    
    SY_DBG("%s: result = %s\n",__func__, (char *)XU_RSP_Packet.data);

    return SUCCESS;
}

/**
 * @brief 
 * 
 * @param args 
 * @return int 
 */
static int function_SoftReset(cam_operation_args_t * args)
{
    // SY_DBG("%s: Start\n", __func__);
    if(*args->hDev == NULL)
        SY_CHECK(!uvc_open(args->hDev,args->cam_dev), return FAILURE);
    
    SY_CHECK(!uvc_startStream(*args->hDev, args->cam_dev), return FAILURE);
    
    usleep(1000 * 1000); // 1s delay

    SY_CHECK(!uvc_stopStream(*args->hDev, NULL), return FAILURE);
    
    SY_CHECK(!uvc_close(*args->hDev), return FAILURE);
    *args->hDev = NULL;

    // SY_DBG("%s: End\n", __func__);

    return SUCCESS;
}

static int function_SwitchOnOff(cam_operation_args_t * args)
{
    // SY_DBG("%s: Start\n", __func__);

    int32_t timeCount = *(uint32_t *) args->data;
    SY_CHECK(args->data_len == sizeof(timeCount), return FAILURE);

    if(*args->hDev == NULL)
    {   
        SY_DBG("%s: hDev is NULL\n", __func__);
        SY_CHECK(!uvc_open(args->hDev,args->cam_dev), return FAILURE);
    }   
    SY_CHECK(!uvc_startStream(*args->hDev, args->cam_dev), return FAILURE);
    
    usleep(1000 * 1000); // 1s delay

    SY_CHECK(!uvc_stopStream(*args->hDev, NULL), return FAILURE);

    // SY_DBG("%s: End\n", __func__);

    return SUCCESS;
}

/**
 * @brief Wrapper for function thead
 * 
 * @param p 
 * @return void* 
 */
static void * function_ThreadWrapper(void *p)
{
    cam_operation_args_t * args = (cam_operation_args_t *)p;
    int32_t timeCount = 0;
    int32_t timeLimit = *(uint32_t *) args->data;
    SY_CHECK(args->data_len == sizeof(timeLimit), return NULL);
    SY_CHECK(timeLimit > 0, SY_ERR("timeList must be bigger than 0\n");return NULL);
    SY_DBG("start Thread %s, timeLimit = %d\n", args->func_attr->function_name, timeLimit);

    uint32_t *ThreadStart = &args->func_attr->ThreadStart;
    cam_operation_handle_t handle = *args->phandle;
    char * handle_name = args->cam_dev->operationes.operation_list[args->op_idx].name;

    while(*ThreadStart == TRUE)
    {
        timeCount ++;
        SY_DBG("%s: start %d test\n", args->func_attr->function_name, timeCount);
        SY_CHECK(!handle(args), SY_ERR("exec %s -> %s error\n", args->cam_dev->name, handle_name);
                                return NULL);
        if(timeLimit-- <= 0)
        {
            *ThreadStart = FALSE;
            SY_DBG("%s -> Thread %s Exit, test %d times\n", args->cam_dev->name, handle_name, timeCount);
            continue;
        }
    }
    return NULL;
}
/***************************************************************************** FUNCTION HANDLE ***********************************************************/


/*****************************************************************************  FUNCTION TABLE ***********************************************************/
static function_attr_t function_supported[] ={
    {"StartCapture", function_StartCapture, FUNCTION_EXEC_ONCE}, 
    {"StopCapture",  function_StopCapture,  FUNCTION_EXEC_ONCE},
    {"CaptureFrame", function_CaptureFrame, FUNCTION_EXEC_ONCE},
    {"GetDeviceSN",  function_GetDeviceSN,  FUNCTION_EXEC_ONCE},
    {"GetFirmwareVersion",  function_GetFirmwareVersion,  FUNCTION_EXEC_ONCE},
    {"SoftReset",    function_SoftReset,    FUNCTION_EXEC_LOOP},
    {"SwitchOnOff",  function_SwitchOnOff,  FUNCTION_EXEC_LOOP},
};
/***************************************************************************** FUNCTION TABLE *********************************************************/


/***************************************************************************** EXPOSETED INTERFACE ******************************************************/
/**
 * @brief 
 * 
 * @param handle_name 
 * @param functionAttr 
 * @return int 
 */
int function_GetFunctionAttrWithName(const char * handle_name, function_attr_t ** functionAttr)
{
    for(int i = 0; i < ARRAY_SIZE(function_supported); i++)
    {
        if(!strcmp(function_supported[i].function_name, handle_name))
        {
            // SY_DBG("handle %s is found\n", handle_name);
            *functionAttr = &function_supported[i];
            return SUCCESS;
        }
    }

    SY_ERR("handle %s is not found\n", handle_name);
    return FAILURE;
}

/**
 * @brief 
 * 
 * @param hanlde_name 
 * @param handle 
 * @return int 
 */
int function_GetHandleWithName(const char * handle_name, cam_operation_handle_t *handle)
{
    for(int i = 0; i < ARRAY_SIZE(function_supported); i++)
    {
        if(!strcmp(function_supported[i].function_name, handle_name))
        {
            *handle = function_supported[i].op_handle;
            return SUCCESS;
        }
    }

    SY_ERR("handle %s is not found\n", handle_name);
    return FAILURE;
}

/**
 * @brief 
 * 
 * @param handle_name 
 * @param phDev 
 * @param pstCam 
 * @param op_idx 
 * @return int 
 */
int function_ExecHandleByName(const char *handle_name, HCAM *phDev, cam_dev_t *pstCam, uint32_t op_idx)
{
    function_attr_t *op_func_attr = NULL;
    cam_operation_args_t *op_args;
    cam_operation_config_t *op_config;

    SY_CHECK(!function_GetFunctionAttrWithName(handle_name, &op_func_attr), return FAILURE);

    op_args = &pstCam->operationes.operation_list[op_idx].args;
    op_config = &pstCam->operationes.operation_list[op_idx].config;

    op_args->func_attr= op_func_attr;
    op_args->phandle = &op_func_attr->op_handle;

    op_args->cam_dev = pstCam;
    op_args->hDev = phDev;
    op_args->op_idx = op_idx;
    // TODO reliable sources of data
    op_args->data_len = sizeof(op_config->data);
    op_args->data = (uint8_t *)malloc(op_args->data_len);
    memcpy(op_args->data, &op_config->data, op_args->data_len);
    
    SY_DBG("%s: start to exec %s\n", __func__, op_func_attr->function_name);
    switch(op_func_attr->function_type) {
        case FUNCTION_EXEC_ONCE:
            SY_CHECK(!op_func_attr->op_handle(op_args), return FAILURE);
            FREE(op_args->data);
            break;
        case FUNCTION_EXEC_LOOP:
            if(op_func_attr->ThreadStart == TRUE)
            {
                SY_DBG("Switch OnOff Stop\n");
                op_func_attr->ThreadStart = FALSE;
                pthread_join(op_func_attr->Tid, NULL);
            }
            else
            {
                SY_DBG("Switch OnOff Start\n");
                op_func_attr->ThreadStart = TRUE;
                pthread_create(&op_func_attr->Tid, NULL, function_ThreadWrapper, op_args);
                pthread_setname_np(op_func_attr->Tid, op_func_attr->function_name);
            }
            break;
        default:
            SY_ERR("Wrong function type %d\n", op_func_attr->function_type);
            return FAILURE;
            break;
    }
    return SUCCESS;
}

/**
 * @brief 
 * 
 * @return int 
 */
int function_Deinit(void)
{
    for(int i = 0; i < ARRAY_SIZE(function_supported); i++)
    {
        if(function_supported[i].function_type == FUNCTION_EXEC_LOOP)
        {
            function_supported[i].ThreadStart = TRUE;
            pthread_join(function_supported[i].Tid, NULL);
        }
    }

    return SUCCESS;
}

/***************************************************************************** EXPOSETED INTERFACE ******************************************************/