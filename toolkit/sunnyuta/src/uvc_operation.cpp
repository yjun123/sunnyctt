/**
 * @file uvc_operation.cpp
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief uvc related code
 * @version 0.1
 * @date 2023-06-28
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */

#include "sunnyuta.h"
#include "sunny_uvc_sdk.h"
#include "debug.h"
#include "uvc_operation.h"
#include <thread>
#include "tool.c"
#if PALM_DEVICE_XU_ENABLE
#include "palm.h"
#endif



static int g_frameCnt = 0;
static int g_frameCaptureFlag = 0;

static void fnOnFrame(void *buffer, int buf_len, int width, int height, unsigned int format,
                                   unsigned long long timestamp, void *user_data)
{
	g_frameCnt ++;
	cam_dev_t * dev = (cam_dev_t *)user_data;

	printf("frameIdx %d fnOnFrame buf_len:%d ----------\n", g_frameCnt, buf_len);

	if(g_frameCaptureFlag)
	{
		time_t t = time(0);
		char time_s[9] = {0};
		strftime(time_s, 9, "%H:%M:%S", localtime(&t));
		
		char dumpFileName[256];
		sprintf(dumpFileName, "%s-%dx%d-%X-%s.bin", dev->name, width, height, format, time_s);
		SY_DBG("%s\n", dumpFileName);
		sy_misc_file_simple_write(dumpFileName, (char *)buffer, buf_len);
		
		g_frameCaptureFlag = 0;
	}
}

static int testCount = 0;

/**
 * @brief UVC relatied initialization
 * 
 * @return int 
 */
int uvc_init(void)
{
	SY_CHECK(!Sunny_SDK_Init(), return FAILURE);

	return SUCCESS;
}

/**
 * @brief uVC relatied deinitialization
 * 
 * @return int 
 */
int uvc_deinit(void)
{
	SY_CHECK(!Sunny_SDK_Uninit(), );
	
	return SUCCESS;
}

/**
 * @brief check uvc device
 * 
 * @param dev 
 * @return int 
 */
int uvc_is_existed(cam_dev_t *dev)
{
	int devnum;
	CamDev * cam = NULL;
	CamDev * camdev = (CamDev *) malloc(sizeof(CamDev) *  SUNNY_DEV_MAX_CNT);


	int ret = Sunny_SearchDevice(camdev, &devnum);
	if(ret == 0)
	{
		for(int i=0; i< devnum; i++)
		{
			if(camdev[i].vid == dev->usb_id.vid && camdev[i].pid == dev->usb_id.pid)
			{ 
				cam = camdev + i;
				break;
			}
		}
		if(cam == NULL)
		{
			SY_DBG("dev %s not found\n", dev->name);
			goto __fail;
		}
	}


	FREE(camdev);
	return SUCCESS;

__fail:
	FREE(camdev);
	return FAILURE;
}

/**
 * @brief UVC open deivice operatioin
 * 
 * @param dev 
 * @param hDev 
 * @return int 
 */
int uvc_open(HCAM *hDev, cam_dev_t *dev)
{
	CamDev * camdev = (CamDev *) malloc(sizeof(CamDev) *  SUNNY_DEV_MAX_CNT);

	//char campath[] = "/dev/bus/usb/001/006";
	/*
	char campath[256] = {};
	sprintf(campath, "/dev/bus/usb/%s/%s", cbusNum, cdevNum);

	int fd = open(campath, O_RDWR);
	if (fd < 0)
	{
		printf("testdemo: open fd failed,fd=%d\n", fd);
		break;
	}
	
	*/
	int devnum;
	CamDev * cam = NULL;
	int ret = Sunny_SearchDevice(camdev, &devnum);
	if(ret == 0)
	{
		for(int i=0; i< devnum; i++)
		{
			printf("vid: %d\n", (camdev+i)->vid);
			printf("pid: %d\n", (camdev+i)->pid);
			if((camdev+i)->vid == dev->usb_id.vid && (camdev+i)->pid == dev->usb_id.pid)
			{ 
				cam = camdev + i;
				break;
			}
		}
		if(cam == NULL)
		{
			printf("dev not found");
		}
	}

	SY_CHECK(!Sunny_Open(hDev, cam), SY_ERR("open %s failed\n", dev->name);
										goto __fail);
	
	FREE(camdev);
	return SUCCESS;

__fail:
	FREE(camdev);
	return FAILURE;
}

/**
 * @brief 
 * 
 * @param hDev 
 * @return int 
 */
int uvc_close(HCAM hDev)
{
	SY_CHECK(!Sunny_Close(hDev), return FAILURE);

	return SUCCESS;
}
/**
 * @brief UVC start stream
 * 
 * @param hDev 
 * @param dev 
 * @return int 
 */

int uvc_startStream(HCAM hDev, cam_dev_t *dev)
{
	CamRes camres;
	camres.width= dev->video_attr.width;
	camres.height= dev->video_attr.height;
	camres.frameRate= dev->video_attr.frameRate;
	camres.format= dev->video_attr.format;
	int ret = Sunny_StartStream(hDev, &camres,fnOnFrame, dev);
	if (ret != 0)
	{
		printf("testdemo: startStream failed,ret=%d\n", ret);
		return FAILURE;
	}
	/*
	ret = startStream(hDev, TYPE_IR);
	if (ret != SUNNY_RET_SUCCESS)
	{
		printf("testdemo: startStream Ir failed,ret=%d\n", ret);
		break;
	}
	usleep(1 * 100);
	
	ret = stopStream(hDev, TYPE_IR);
	if (ret != SUNNY_RET_SUCCESS)
	{
		printf("testdemo: stopStream Ir failed,ret=%d\n", ret);
		break;
	}
	ret = stopStream(hDev, TYPE_RGB);
	if (ret != SUNNY_RET_SUCCESS)
	{
		printf("testdemo: stopStream Rgb failed,ret=%d\n", ret);
		break;
	}
	printf("start close\n");
	ret = Close(hDev);
	if (ret != SUNNY_RET_SUCCESS)
	{
		printf("testdemo: Close failed,ret=%d\n", ret);
		break;
	}
	close(fd);
	fd = -1;
	frameCnt = 0;

	printf("testdemo: test count=%d\n", testCount++);
	*/

	return SUCCESS;
}

/**
 * @brief UVC stop stream
 * 
 * @param hDev 
 * @param dev 
 * @return int 
 */
int uvc_stopStream(HCAM hDev, cam_dev_t *dev __attribute__((unused)))
{
	SY_CHECK(!Sunny_StopStream(hDev), return FAILURE);
	return SUCCESS;
}

/**
 * @brief 
 * 
 * @param hDev 
 * @param __attribute__ 
 * @return int 
 */
int uvc_captureFrame(HCAM hDev, cam_dev_t *dev __attribute__((unused)))
{
	g_frameCaptureFlag = 1;
	return SUCCESS;
}

/**
 * @brief 
 * 
 * @param hDev 
 * @param __attribute__ 
 * @param args 
 * @param XU_CMD_Packet 
 * @param CMD_Packet_size 
 * @param XU_RSP_Packet 
 * @param RSP_Packet_size 
 * @return int 
 */
int uvc_controlXU(HCAM hDev, cam_dev_t *dev __attribute__((unused)), cam_operation_config_t *args, UVC_XU_CMD_Packet_t *XU_CMD_Packet, ssize_t CMD_Packet_size, UVC_XU_RSP_Packet_t * XU_RSP_Packet, ssize_t RSP_Packet_size)
{
	uint recvLeng = 0;
	SoUVCExtUnitDescriptor Config;
	
	SY_CHECK(!strcmp(args->type, "XU"),SY_ERR("the type of args is wrong, %s\n", args->type);
									return FAILURE);

	Config.bUnitID = args->uint_id;
	SY_CHECK(!Sunny_SetVcExtUnitConfig(hDev, &Config), return FAILURE);

	SY_CHECK(!Sunny_VcExtUnitTransfer(hDev, args->control_selector, XU_CMD_Packet, CMD_Packet_size, 0, &recvLeng), return FAILURE);

	SY_CHECK(!Sunny_VcExtUnitTransfer(hDev, args->control_selector, XU_RSP_Packet, RSP_Packet_size, 1, &recvLeng), return FAILURE);

	// SY_CHECK(recvLeng == RSP_Packet_size ,SY_ERR("%s: recv data len = %d\n",__func__, recvLeng);
	// 									 return FAILURE);
	
	return SUCCESS;
}