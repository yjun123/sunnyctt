/**
 * @file ui_interaction.cpp
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief uvc related code
 * @version 0.1
 * @date 2023-06-28
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */

#include "sunnyuta.h"
#include "debug.h"
#include "uvc_operation.h"
#include "ui_interaction.h"

#if defined(USING_NCURSES) && USING_NCURSES == 0
/**
 * @brief demo,only get one number
 * 
 * @param tips 
 * @param string_format 
 * @param d 
 * @return int 
 */
int ui_get_single_number(const char * tips, const char * string_format, void *d)
{
    printf("%s", tips);
    scanf(string_format, (int32_t *)d);

    return SUCCESS;
}

// int ui_get_input( char * tips, char * string_format)
// {
//     printf("%s\n", tips);
//     scanf(string_format, &)
// }

/**
 * @brief show all device which is detected
 * 
 * @param dev 
 * @param dev_num 
 * @return int 
 */
int ui_show_device(cam_dev_t *dev, uint32_t dev_num)
{
    SY_CHECK(dev != NULL && dev_num > 0, goto __fail);

    INFO("please select device:\n");

    INFO("IDX   NAME       VID     PID\n");

    for(int i = 0; i < dev_num; i++)
    {
        if(dev[i].ok)
            printf("%d.    %-10s 0x%04X  0x%04X\n", 
                    i, dev[i].name, dev[i].usb_id.vid, dev[i].usb_id.pid);
    }
    printf("%d.    %-10s 0x%s  0x%s\n", dev_num, "Refresh", "DEAD", "BEEF");
    return SUCCESS;
__fail:

    return FAILURE;
}

/**
 * @brief show device operation
 * 
 * @param dev 
 * @return int 
 */
int ui_show_operation(cam_dev_t *dev)
{
    SY_CHECK(dev != NULL, goto __fail);

    INFO("Operations of  %s device:\n", dev->name);

    INFO("IDX    METHOD\n");
    for(int i = 0; i < dev->operationes.supported_opt_num; i++)
    {
        printf("%d.     %-10s\n", 
                i, dev->operationes.operation_list[i].name);
    }

    return SUCCESS;
__fail:

    return FAILURE;
}

#else
static void finish(int sig);

/**
 * @brief UI Initialization
 * 
 * @return int 
 */
int ui_init(void)
{
    int num = 0;

    /* initialize your non-curses data structures here */

    (void) signal(SIGINT, finish);      /* arrange interrupts to terminate */

    (void) initscr();      /* initialize the curses library */
    (void) raw();
    // keypad(stdscr, TRUE);  /* enable keyboard mapping */
    // (void) nonl();         /* tell curses not to do NL->CR/NL on output */
    // (void) cbreak();       /* take input chars one at a time, no wait for \n */
    (void) noecho();         /* echo input - in color */
    curs_set(0);

#if 0
    if (has_colors())
    {
        start_color();

        /*
         * Simple color assignment, often all we need.  Color pair 0 cannot
         * be redefined.  This example uses the same value for the color
         * pair as for the foreground color, though of course that is not
         * necessary:
         */
        init_pair(1, COLOR_RED,     COLOR_BLACK);
        init_pair(2, COLOR_GREEN,   COLOR_BLACK);
        init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
        init_pair(4, COLOR_BLUE,    COLOR_BLACK);
        init_pair(5, COLOR_CYAN,    COLOR_BLACK);
        init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
        init_pair(7, COLOR_WHITE,   COLOR_BLACK);
    }


    for (;;)
    {
        int c = getch();     /* refresh, accept single keystroke of input */
        attrset(COLOR_PAIR(num % 8));
        num++;

        /* process the command keystroke */
    }
#endif

    const char * title = "SunnyUTA";
    mvprintw(LINES/2, (COLS-strlen(title))/2, title);
    refresh();

    getch();

    finish(0);               /* we are done */

    return 0;
}

static void finish(int sig)
{
    endwin();

    /* do your non-curses wrapup here */

    // exit(0);
}
#endif