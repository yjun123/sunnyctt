/**
 * @file palm.h
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief 
 * @version 0.1
 * @date 2023-07-26
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */

#include "sunnyuta.h"

#pragma pack(1)

// 命令包格式 60byte
typedef struct 
{
    uint8_t head[2];            // 命令包头 固定0x55 0xAA
    uint8_t cmd;                // 命令码
    uint8_t len;                // 有效数据长度
    uint8_t data[55];           // 数据包
    uint8_t cks;                // 校验和
}PALM_XU_CMD_Packet_t;

// 应答包格式 60byte
typedef struct 
{
    uint8_t head[2];            // 应答包头 固定0xAA 0x55
    uint8_t rsp;                // 应答码
    uint8_t len;                // 有效数据长度
    uint8_t data[55];           // 数据包
    uint8_t cks;                // 校验和
}PALM_XU_RSP_Packet_t;

/** @func   : XU_CheckCmdPacket
 *  @brief  : 校验命令包
 *  @param  : packet[out] -> 命令包
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_CheckCmdPacket(PALM_XU_CMD_Packet_t *packet);

/** @func   : XU_BuildRspPacket
 *  @brief  : 创建应答包
 *  @param  : packet[out] -> 命令包
 *            rsp[in] -> 应答码
 *            data[in] -> 数据
 *            len[in] -> 数据长度
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_BuildRspPacket(PALM_XU_RSP_Packet_t *packet, uint8_t rsp, uint8_t *data, uint8_t len);

/** @func   : XU_BuildCmdPacket
 *  @brief  : 创建命令包
 *  @param  : packet[out] -> 命令包
 *            cmd[in] -> 命令码
 *            data[in] -> 数据
 *            len[in] -> 数据长度
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_BuildCmdPacket(PALM_XU_CMD_Packet_t *packet, uint8_t cmd, uint8_t *data, uint8_t len);

/** @func   : XU_CheckRspPacket
 *  @brief  : 校验命令包
 *  @param  : packet[out] -> 命令包
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_CheckRspPacket(PALM_XU_RSP_Packet_t *packet);