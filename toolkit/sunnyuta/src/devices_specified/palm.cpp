
/**
 * @file palm.cpp
 * @author zngyanj (zngyanj@sunnyoptical.com)
 * @brief device speicied for Palm Device (Sunnypalm/WePalm)
 * @version 0.1
 * @date 2023-07-26
 * 
 * ©2023 Zhejiang Sunny Intelligent Optical Technology Co., Ltd. All Rights Reserved
 * 
 */
#include "palm.h"

/** @func   : XU_CheckRspPacket
 *  @brief  : 校验命令包
 *  @param  : packet[out] -> 命令包
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_CheckRspPacket(PALM_XU_RSP_Packet_t *packet)
{
    uint8_t cks = 0;
    uint8_t *p = (uint8_t *)packet;

    if(packet->head[0] != 0xAA || packet->head[1] != 0x55)
    {
        return 1;
    }

    for(uint32_t i = 0; i < sizeof(PALM_XU_RSP_Packet_t) - 1; i++)
    {
        cks += p[i];
    }

    if(packet->cks != cks)
    {
        return 1;
    }

    return 0;
}


/** @func   : XU_CheckCmdPacket
 *  @brief  : 校验命令包
 *  @param  : packet[out] -> 命令包
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_CheckCmdPacket(PALM_XU_CMD_Packet_t *packet)
{
    uint8_t cks = 0;
    uint8_t *p = (uint8_t *)packet;

    if(packet->head[0] != 0x55 || packet->head[1] != 0xAA)
    {
        return 1;
    }

    for(uint32_t i = 0; i < sizeof(PALM_XU_CMD_Packet_t) - 1; i++)
    {
        cks += p[i];
    }

    if(packet->cks != cks)
    {
        return 1;
    }

    return 0;
}

/** @func   : XU_BuildRspPacket
 *  @brief  : 创建应答包
 *  @param  : packet[out] -> 命令包
 *            rsp[in] -> 应答码
 *            data[in] -> 数据
 *            len[in] -> 数据长度
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_BuildRspPacket(PALM_XU_RSP_Packet_t *packet, uint8_t rsp, uint8_t *data, uint8_t len)
{
    uint8_t cks = 0;
    uint8_t *p = (uint8_t *)packet;

    if(packet == NULL || (data == NULL && len != 0) || len > 55)
    {
        return 1;
    }

    memset(packet, 0, sizeof(PALM_XU_CMD_Packet_t));

    packet->head[0] = 0xAA;
    packet->head[1] = 0x55;
    packet->rsp     = rsp;
    packet->len     = len;
    memcpy(packet->data, data, len);
    for(uint32_t i = 0; i < sizeof(PALM_XU_CMD_Packet_t) - 1; i++)
    {
        cks += p[i];
    }
    packet->cks = cks;

    return 0;
}

/** @func   : XU_BuildCmdPacket
 *  @brief  : 创建命令包
 *  @param  : packet[out] -> 命令包
 *            cmd[in] -> 命令码
 *            data[in] -> 数据
 *            len[in] -> 数据长度
 *  @return : 0 -> 成功 1 -> 失败
 */
int PALM_XU_BuildCmdPacket(PALM_XU_CMD_Packet_t *packet, uint8_t cmd, uint8_t *data, uint8_t len)
{
    uint8_t cks = 0;
    uint8_t *p = (uint8_t *)packet;

    if(packet == NULL || (data == NULL && len != 0)  || len > 55)
    {
        return 1;
    }

    memset(packet, 0, sizeof(PALM_XU_CMD_Packet_t));

    packet->head[0] = 0x55;
    packet->head[1] = 0xAA;
    packet->cmd     = cmd;
    packet->len     = len;
    memcpy(packet->data, data, len);
    for(uint32_t i = 0; i < sizeof(PALM_XU_CMD_Packet_t) - 1; i++)
    {
        cks += p[i];
    }
    packet->cks = cks;

    return 0;
}

