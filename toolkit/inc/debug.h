
#ifndef _TOOL_DEBUG_H_
#define _TOOL_DEBUG_H_

#define SUCCESS 0
#define FAILURE (-1)

#undef TRUE
#define TRUE    1

#undef FALSE
#define FALSE   0

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) ((sizeof(a) / sizeof(a[0])))
#endif

#define P_CLR_NONE "\033[0m"
#define P_CLR_RED "\033[0;31m"
#define P_CLR_BLUE "\033[0;34m"

#define INFO(string, args...)  printf("\033[33m" string "\033[0m", ##args)
#define SY_DBG(string, args...) printf("\033[34m" string "\033[0m", ##args)
#define SY_ERR(string, args...) \
	printf("\033[0;31m [%s %d]exec func:" string "\033[0m", __func__, __LINE__,##args)

#define SY_CHECK(b, action) do {if(unlikely(!(b))) {SY_ERR("%s \n", #b); action;}} while(0)

#define CLOSE(x) do{if(x > 0) {close(x); (x) = -1; } } while(0)
#define FCLOSE(x) do{if(x) {fclose(x); (x) = NULL; } } while(0)

#define FREE(x) do { if(x) { free(x); x = NULL; } } while(0)

#endif
